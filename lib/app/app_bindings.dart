import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'core/platform/network_info.dart';
import 'core/util/input_converter.dart';

class AppBindings extends Bindings {
  @override
  void dependencies() async {
    //! External
    final sharedPreference = await SharedPreferences.getInstance();
    Get.put(sharedPreference);
    Get.put(http.Client());
    Get.put(DataConnectionChecker());

    //! Core
    Get.put(InputConverter());
    Get.put(NetworkInfoImpl(Get.find()));
  }
}

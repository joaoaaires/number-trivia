import 'package:flutter/material.dart';
import 'package:flutterva/app/features/number_trivia/presentation/widgets/trivia_display.dart';
import 'package:get/get.dart';

import '../controller/number_trivia_controller.dart';
import '../widgets/loading_widget.dart';
import '../widgets/message_display.dart';
import '../widgets/trivia_controls.dart';

class NumberTriviaPage extends GetView<NumberTriviaController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Number Trivia'),
      ),
      body: SingleChildScrollView(
        child: buildBody(context),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            SizedBox(height: 10),
            Obx(() {
              final error = controller.rxError.value;
              final loading = controller.isLoading.value;
              final numbertrivia = controller.rxNumberTrivia.value;

              if (loading) {
                return LoadingWidget();
              }

              if (numbertrivia != null) {
                return TriviaDisplay(
                  numberTrivia: numbertrivia,
                );
              }

              if (error != null) {
                return MessageDisplay(
                  message: error,
                );
              }

              return MessageDisplay(
                message: 'Start searching.',
              );
            }),
            SizedBox(height: 20),
            TriviaControls(),
          ],
        ),
      ),
    );
  }

/*
  BlocProvider<NumberTriviaBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (context) => sl<NumberTriviaBloc>(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(height: 10),
              BlocBuilder<NumberTriviaBloc, NumberTriviaState>(
                builder: (_, state) {
                  if (state is Empty) {
                    return MessageDisplay(
                      message: 'Start searching.',
                    );
                  } else if (state is Loading) {
                    return LoadingWidget();
                  } else if (state is Loaded) {
                    return TriviaDisplay(
                      numberTrivia: state.trivia,
                    );
                  } else if (state is Error) {
                    return MessageDisplay(
                      message: state.message,
                    );
                  }

                  return Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: Center(
                      child: Text('Start searching.'),
                    ),
                  );
                },
              ),
              SizedBox(height: 20),
              TriviaControls(),
            ],
          ),
        ),
      ),
    );
  }
  */
}

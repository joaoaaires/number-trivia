import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../routes/app_routes.dart';
import '../controller/number_trivia_controller.dart';

class TriviaControls extends StatefulWidget {
  const TriviaControls({Key key}) : super(key: key);

  @override
  _TriviaControlsState createState() => _TriviaControlsState();
}

class _TriviaControlsState extends State<TriviaControls> {
  final controller = TextEditingController();
  String inputStr;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: controller,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Input a number',
          ),
          onChanged: (value) {
            inputStr = value;
          },
          onSubmitted: (_) {
            addConcrete();
          },
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                child: Text('Search'),
                style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).accentColor,
                ),
                onPressed: addConcrete,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: ElevatedButton(
                child: Text('Get random trivia'),
                style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).accentColor,
                ),
                onPressed: addRandom,
              ),
            ),
          ],
        ),
        ElevatedButton(
          onPressed: () => Get.toNamed(AppRoutes.PHOTO),
          child: Text("Go Camera"),
        ),
      ],
    );
  }

  addConcrete() {
    controller.clear();

    NumberTriviaController c = Get.find();
    c.getTriviaForConcreteNumber(inputStr);

    inputStr = null;
  }

  addRandom() {
    controller.clear();

    NumberTriviaController c = Get.find();
    c.getTriviaForRandomNumber();

    inputStr = null;
  }
}

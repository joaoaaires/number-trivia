import 'package:get/get.dart';
import 'package:meta/meta.dart';

import '../../../../core/usecases/usecase.dart';
import '../../../../core/util/input_converter.dart';
import '../../domain/entities/number_trivia.dart';
import '../../domain/usecases/get_concrete_number_trivia.dart';
import '../../domain/usecases/get_random_number_trivia.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class NumberTriviaController extends GetxController {
  final GetConcreteNumberTrivia getConcreteNumberTrivia;
  final GetRandomNumberTrivia getRandomNumberTrivia;
  final InputConverter inputConverter;

  final rxNumberTrivia = Rxn<NumberTrivia>();
  final rxError = Rxn<String>();
  final isLoading = false.obs;

  NumberTriviaController({
    @required GetConcreteNumberTrivia concrete,
    @required GetRandomNumberTrivia random,
    @required this.inputConverter,
  })  : assert(concrete != null),
        assert(random != null),
        assert(inputConverter != null),
        getConcreteNumberTrivia = concrete,
        getRandomNumberTrivia = random;

  void reset() {
    rxNumberTrivia.value = null;
    isLoading.value = false;
    rxError.value = null;
  }

  void getTriviaForConcreteNumber(String numberString) async {
    isLoading.value = true;
    final inputEither = inputConverter.stringToUnsignedInteger(numberString);
    inputEither.fold(
      (l) => updateValues(error: INVALID_INPUT_FAILURE_MESSAGE),
      (integer) async {
        final failureOrTrivia = await getConcreteNumberTrivia(
          Params(number: integer),
        );
        failureOrTrivia.fold(
          (l) => updateValues(error: SERVER_FAILURE_MESSAGE),
          (r) => updateValues(numberTrivia: r),
        );
      },
    );
  }

  void getTriviaForRandomNumber() async {
    isLoading.value = true;
    final failureOrTrivia = await getRandomNumberTrivia(
      NoParams(),
    );
    failureOrTrivia.fold(
      (l) => updateValues(error: SERVER_FAILURE_MESSAGE),
      (r) => updateValues(numberTrivia: r),
    );
  }

  void updateValues({
    NumberTrivia numberTrivia,
    String error,
    bool loading: false,
  }) {
    rxNumberTrivia.value = numberTrivia;
    isLoading.value = loading;
    rxError.value = error;
  }
}

//   NumberTriviaBloc({
//     @required GetConcreteNumberTrivia concrete,
//     @required GetRandomNumberTrivia random,
//     @required this.inputConverter,
//   })  : assert(concrete != null),
//         assert(random != null),
//         assert(inputConverter != null),
//         getConcreteNumberTrivia = concrete,
//         getRandomNumberTrivia = random,
//         super(Empty());

//   @override
//   Stream<NumberTriviaState> mapEventToState(
//     NumberTriviaEvent event,
//   ) async* {
//     if (event is GetTriviaForConcreteNumber) {
//       final inputEither =
//           inputConverter.stringToUnsignedInteger(event.numberString);

//       yield* inputEither.fold(
//         (failure) async* {
//           yield Error(message: INVALID_INPUT_FAILURE_MESSAGE);
//         },
//         (integer) async* {
//           yield Loading();
//           final failureOrTrivia = await getConcreteNumberTrivia(
//             Params(number: integer),
//           );
//           yield* _eitherLoadedOrErrorState(failureOrTrivia);
//         },
//       );
//     } else if (event is GetTriviaForRandomNumber) {
//       yield Loading();
//       final failureOrTrivia = await getRandomNumberTrivia(NoParams());
//       yield* _eitherLoadedOrErrorState(failureOrTrivia);
//     }
//   }

//   Stream<NumberTriviaState> _eitherLoadedOrErrorState(
//     Either<Failure, NumberTrivia> failureOrTrivia,
//   ) async* {
//     yield failureOrTrivia.fold(
//       (failure) => Error(message: _mapFailureToMessage(failure)),
//       (trivia) => Loaded(trivia: trivia),
//     );
//   }

//   String _mapFailureToMessage(Failure failure) {
//     switch (failure.runtimeType) {
//       case ServerFailure:
//         return SERVER_FAILURE_MESSAGE;
//       case CacheFailure:
//         return CACHE_FAILURE_MESSAGE;
//       default:
//         return 'Unexpected error';
//     }
//   }
// }
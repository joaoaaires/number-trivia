import 'package:get/get.dart';

import '../../data/datasources/number_trivia_local_data_source.dart';
import '../../data/datasources/number_trivia_remote_data_source.dart';
import '../../data/repositories/number_trivia_repository_impl.dart';
import '../../domain/repositories/number_trivia_repository.dart';
import '../../domain/usecases/get_concrete_number_trivia.dart';
import '../../domain/usecases/get_random_number_trivia.dart';
import 'number_trivia_controller.dart';

class NumberTriviaBindings extends Bindings {
  @override
  void dependencies() {
    // Data sources
    Get.lazyPut<NumberTriviaRemoteDataSource>(
      () => NumberTriviaRemoteDataSourceImpl(client: Get.find()),
    );

    Get.lazyPut<NumberTriviaLocalDataSource>(
      () => NumberTriviaLocalDataSourceImpl(sharedPreferences: Get.find()),
    );

    // Repository
    Get.lazyPut<NumberTriviaRepository>(
      () => NumberTriviaRepositoryImpl(
        remoteDataSource: Get.find(),
        localDataSource: Get.find(),
        networkInfo: Get.find(),
      ),
    );

    // Use cases
    Get.lazyPut(() => GetConcreteNumberTrivia(Get.find()));
    Get.lazyPut(() => GetRandomNumberTrivia(Get.find()));

    // Controller
    Get.lazyPut(() => NumberTriviaController(
          concrete: Get.find(),
          random: Get.find(),
          inputConverter: Get.find(),
        ));
  }
}

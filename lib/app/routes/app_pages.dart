import 'package:get/get.dart';

import '../features/number_trivia/presentation/controller/number_trivia_bindings.dart';
import '../features/number_trivia/presentation/pages/number_trivia_page.dart';
import '../features/number_trivia/presentation/photo/photo_page.dart';
import 'app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: AppRoutes.NUMBER_TRIVIA,
      page: () => NumberTriviaPage(),
      binding: NumberTriviaBindings(),
    ),
    GetPage(
      name: AppRoutes.PHOTO,
      page: () => PhotoPage(),
    )
  ];
}

abstract class AppRoutes {
  static const INITIAL = '/';
  static const NUMBER_TRIVIA = '/number';
  static const PHOTO = '/photo';
}

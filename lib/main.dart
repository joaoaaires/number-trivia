import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/app_widget.dart';
import 'injection_container.dart';

//// Plugin do sonar https://github.com/insideapp-oss/sonar-flutter/tree/0.3.2
/// Tutorial https://www.youtube.com/watch?v=QD5J8YvQPPM
/// versão do sonar 8.7.1
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sharedPreference = await SharedPreferences.getInstance();
  InjectionContainer(sharedPreference).dependencies();
  runApp(AppWidget());
}

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'app/core/platform/network_info.dart';
import 'app/core/util/input_converter.dart';

class InjectionContainer extends Bindings {
  final SharedPreferences sharedPreferences;

  InjectionContainer(this.sharedPreferences);

  @override
  void dependencies() async {
    //! External
    Get.put(sharedPreferences);
    Get.put(http.Client());
    Get.put(DataConnectionChecker());

    //! Core
    Get.put(InputConverter());
    Get.put<NetworkInfo>(NetworkInfoImpl(Get.find()));
  }
}

// import 'package:bloc_test/bloc_test.dart';
// import 'package:dartz/dartz.dart';
// import 'package:flutter_test/flutter_test.dart';
// import 'package:flutterva/app/core/error/failure.dart';
// import 'package:flutterva/app/core/usecases/usecase.dart';
// import 'package:flutterva/app/core/util/input_converter.dart';
// import 'package:flutterva/app/features/number_trivia/domain/entities/number_trivia.dart';
// import 'package:flutterva/app/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
// import 'package:flutterva/app/features/number_trivia/domain/usecases/get_random_number_trivia.dart';

// import 'package:mockito/mockito.dart';

// class MockGetConcreteNumberTrivia extends Mock
//     implements GetConcreteNumberTrivia {}

// class MockGetRandomNumberTrivia extends Mock implements GetRandomNumberTrivia {}

// class MockInputConverter extends Mock implements InputConverter {}

void main() {}
//   MockGetConcreteNumberTrivia mockGetConcreteNumberTrivia;
//   MockGetRandomNumberTrivia mockGetRandomNumberTrivia;
//   MockInputConverter mockInputConverter;

//   setUp(() {
//     mockGetConcreteNumberTrivia = MockGetConcreteNumberTrivia();
//     mockGetRandomNumberTrivia = MockGetRandomNumberTrivia();
//     mockInputConverter = MockInputConverter();
//   });

//   group(
//     'GetTriviaForConcreteNumber',
//     () {
//       final tNumberString = '1';
//       final tNumberParsed = 1;
//       final tNumberTrivia = NumberTrivia(text: 'test trivia', number: 1);

//       setUpMockInputConverterSuccess() =>
//           when(mockInputConverter.stringToUnsignedInteger(any))
//               .thenReturn(Right(tNumberParsed));

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should call the InputConverter to validate and  convert the  string  to an usigned integer',
//         build: () {
//           setUpMockInputConverterSuccess();
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForConcreteNumber(tNumberString)),
//         expect: () => [Loading()],
//         verify: (_) =>
//             verify(mockInputConverter.stringToUnsignedInteger(tNumberString)),
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should emit [Error] when the input is invalid',
//         build: () {
//           when(mockInputConverter.stringToUnsignedInteger(any))
//               .thenReturn(Left(InvalidInputFailure()));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForConcreteNumber(tNumberString)),
//         expect: () => [Error(message: INVALID_INPUT_FAILURE_MESSAGE)],
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should get data from the concrete use case',
//         build: () {
//           setUpMockInputConverterSuccess();
//           when(mockGetConcreteNumberTrivia(any))
//               .thenAnswer((_) async => Right(tNumberTrivia));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForConcreteNumber(tNumberString)),
//         verify: (_) =>
//             verify(mockGetConcreteNumberTrivia(Params(number: tNumberParsed))),
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should emit [Loading, Loaded] when data is gotten successfully',
//         build: () {
//           setUpMockInputConverterSuccess();
//           when(mockGetConcreteNumberTrivia(any))
//               .thenAnswer((_) async => Right(tNumberTrivia));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForConcreteNumber(tNumberString)),
//         expect: () => [
//           Loading(),
//           Loaded(trivia: tNumberTrivia),
//         ],
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should emit [Loading, Error] when getting data fails',
//         build: () {
//           setUpMockInputConverterSuccess();
//           when(mockGetConcreteNumberTrivia(any))
//               .thenAnswer((_) async => Left(ServerFailure()));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForConcreteNumber(tNumberString)),
//         expect: () => [
//           Loading(),
//           Error(message: SERVER_FAILURE_MESSAGE),
//         ],
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should emit [Loading, Error] with a proper message for the error when getting data fails',
//         build: () {
//           setUpMockInputConverterSuccess();
//           when(mockGetConcreteNumberTrivia(any))
//               .thenAnswer((_) async => Left(CacheFailure()));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForConcreteNumber(tNumberString)),
//         expect: () => [
//           Loading(),
//           Error(message: CACHE_FAILURE_MESSAGE),
//         ],
//       );
//     },
//   );

//   group(
//     'GetTriviaForRandomNumber',
//     () {
//       final tNumberTrivia = NumberTrivia(text: 'test trivia', number: 1);

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should get data from the concrete use case',
//         build: () {
//           when(mockGetRandomNumberTrivia(any))
//               .thenAnswer((_) async => Right(tNumberTrivia));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForRandomNumber()),
//         verify: (_) => verify(mockGetRandomNumberTrivia(NoParams())),
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should emit [Loading, Loaded] when data is gotten successfully',
//         build: () {
//           when(mockGetRandomNumberTrivia(any))
//               .thenAnswer((_) async => Right(tNumberTrivia));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForRandomNumber()),
//         expect: () => [
//           Loading(),
//           Loaded(trivia: tNumberTrivia),
//         ],
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should emit [Loading, Error] when getting data fails',
//         build: () {
//           when(mockGetRandomNumberTrivia(any))
//               .thenAnswer((_) async => Left(ServerFailure()));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForRandomNumber()),
//         expect: () => [
//           Loading(),
//           Error(message: SERVER_FAILURE_MESSAGE),
//         ],
//       );

//       blocTest<NumberTriviaBloc, NumberTriviaState>(
//         'should emit [Loading, Error] with a proper message for the error when getting data fails',
//         build: () {
//           when(mockGetRandomNumberTrivia(any))
//               .thenAnswer((_) async => Left(CacheFailure()));
//           return NumberTriviaBloc(
//             concrete: mockGetConcreteNumberTrivia,
//             random: mockGetRandomNumberTrivia,
//             inputConverter: mockInputConverter,
//           );
//         },
//         act: (bloc) => bloc.add(GetTriviaForRandomNumber()),
//         expect: () => [
//           Loading(),
//           Error(message: CACHE_FAILURE_MESSAGE),
//         ],
//       );
//     },
//   );
// }
